public class RenderApp {

    public static void main(String[] args) {
        RenderApp app = new RenderApp();
        app.run();
    }

    private void run() {
        Point a = new Point(0., 1.);
        System.err.println("rotate " + rotate(a, 60));
    }

    private Point rotate(Point a, int angle) {
        Double sinA = Math.sin(2 * Math.PI * angle / 360);
        Double cosA = Math.cos(2 * Math.PI * angle / 360);
        System.err.println("" + sinA + " " + cosA);
        return new Point(a.x * cosA - a.y * sinA, a.x * sinA + a.y * cosA);
    }
}
